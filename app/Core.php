<?php

namespace App;

use App\Services\Db;
use App\Services\Config;
use App\Services\Interfaces\Sourcer;
use App\Services\Pay;
use App\Services\Auth;
use App\Controllers\User;
use App\Models\User as UserModel;

/**
 * Ядро
 */
class Core 
{
    public function run()
    {
        $db = new Db(new Config());
        $userModel = new UserModel($db);
        $auth = new Auth($userModel);
        $pay = new Pay($userModel, $auth);
        $controller = new User($auth, $pay);

        $action = $_GET['action'];

        switch($action) {
            case 'withdraw':
                $controller->withdrawAction();
                break;
            case 'login':
                $controller->loginAction();
                break;
            case 'logout':
                $controller->logoutAction();
                break;
            case 'main':
            default:
                $controller->viewAction('');
        }
    }

}