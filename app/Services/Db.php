<?php
namespace App\Services;

use App\Services\Interfaces\Sourcer;

use PDO;
use Exception;

/**
 * БД class
 */
class Db implements Sourcer {

    /** Инстанс БД */
    private $_db;
    /** Конфиг коннекта */
    private $_configHost;
    private $_configDb;
    private $_configUser;
    private $_configPass;

    public function __construct(Config $config) {
        $this->_configHost = $config::DB_HOST;
        $this->_configDb = $config::DB_NAME;
        $this->_configUser = $config::DB_USER;
        $this->_configPass = $config::DB_PASS;
    }

    public function getConnect() 
    {
        if(is_null($this->_db)) {
            $this->_db = new PDO('mysql:host='.$this->_configHost.';dbname='.$this->_configDb.';charset=utf8mb4', $this->_configUser, $this->_configPass);
        }
        return $this->_db;
    }

    /**
     * Запрос
     *
     * @param string $queryString
     * @return array
     */
    public function query($queryString) {
        $db = $this->getConnect();
        $tmp = $db->prepare($queryString);
        $tmp->execute();
        return $tmp->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Получить строку
     *
     * @param string $table
     * @param string $byField
     * @param string $value
     * @return array
     */
    public function getRow(string $table, string $byField, $byValue)
    {   if (is_string($byValue)) {
            $res = $this->query("SELECT * FROM ".$table . " WHERE ".$byField." = '".$byValue ."' LIMIT 1");
        } else {
            $res = $this->query("SELECT * FROM ".$table . " WHERE ".$byField." = ".$byValue ." LIMIT 1");
        }
        return $res[0];
    }

    /**
     * Обновить поле
     *
     * @param string $table
     * @param string $field
     * @param string $value
     * @return boolean
     */
    public function updateField(string $table, string $field,  $value, string $byField, $byValue): bool
    {

        try {  
            $db = $this->getConnect();

            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->beginTransaction();
            if(is_string($value)) {
                $db->exec("UPDATE ".$table." SET ".$field."='".$value."' WHERE ".$byField."=".$byValue);
            } else {
                $db->exec("UPDATE ".$table." SET ".$field."=".$value." WHERE ".$byField."=".$byValue);
            }
            $db->commit();

            return true;

          } catch (Exception $e) {
            $db->rollBack();

            return false;
          }
    }
}