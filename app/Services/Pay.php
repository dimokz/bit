<?php
namespace App\Services;

use Exception;

class Pay {
    
    private $_user;
    private $_auth;

    public function __construct($user, $auth) {
        $this->_user = $user;
        $this->_auth = $auth;
    }

    /**
     * Undocumented function
     *
     * @param float $amount
     * @return void
     */
    protected function setDeposit(float $amount)
    {
        $this->_user->setData($this->_user::DEPO_FIELD_NAME, (float) $amount);
        $this->_user->save();
    }

    protected function getDeposit(): float
    {
        return (float) $this->_user->getData($this->_user::DEPO_FIELD_NAME);
    }

    /**
     * Списание
     *
     * @param float $amount
     * @return bool
     */
    public function withdraw($amount) 
    {

        if($this->_auth->isLoged()) {
            $currAmount = $this->getDeposit();

            if ($currAmount < $amount) {
                throw new Exception("Недостаток средств");
            }
            $resAmount = $currAmount - $amount;
            $this->setDeposit((float) $resAmount);
        } else {
            throw new Exception("Требуется авторизация");
        }    
    }
}