<?php
namespace App\Services;

use Exception;

/**
 * Класс авторизации и сессии
 */
class Auth {

    private $_user;

    public function __construct($user) {
        $this->_user = $user;
    }

    public function login($login, $password)
    {   

        $this->_user->loadByLogin($login);
        $complexPass = explode(":", $this->_user->getData('password'));

        if (count($complexPass) == 2 && md5($password.$complexPass[1]) == $complexPass[0]) {
            $sessId = session_id();
            $this->_user->setData($this->_user::SESS_FIELD_NAME, (string)$sessId);
            $this->_user->save();
        } else {
            throw new Exception("Ошибка пароля");
        }
    
    }

    public function logout() 
    {
        session_destroy();
    }

    /**
     * Проверка на авторизацию
     *
     * @return boolean
     */
    public function isLoged()
    {
        return $this->getUser()->getData('id') ? true : false;
    }

    public function getUser()
    {  

        if(is_null($this->_user->getData("id"))) {
            $this->_user->loadBySessId(session_id());
        }

        return $this->_user;
    }
}