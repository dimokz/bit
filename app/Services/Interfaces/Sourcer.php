<?php
namespace App\Services\Interfaces;

/**
 * Интерфейс источника данных
 */
interface Sourcer
{
    public function query(string $queryString);

    public function updateField(string $table, string $field, string $value, string $byField, string $byValue): bool;

    public function getRow(string $table, string $byField, $byValue);
}                                                                                                                                               