<?php
namespace App\Models;

use App\Services\Interfaces\Sourcer;

class User 
{
    const TABlE_NAME = "users";
    const DEPO_FIELD_NAME = "amount";
    const LOGIN_FIELD_NAME = "login";
    const PASSWORD_FIELD_NAME = "password";
    const ID_FIELD_NAME = 'id';
    const SESS_FIELD_NAME = 'session';
    const SAVED_FIELDS = [
        self::DEPO_FIELD_NAME,
        self::SESS_FIELD_NAME
    ];

    private $_db;
    private $_id;
    private $_login;
    private $_password;
    private $_amount;
    private $_session;


    public function __construct(Sourcer $db) {
        $this->_db = $db;
    }

    public function loadByLogin(string $login)
    {
        $res = $this->_db->getRow(self::TABlE_NAME, self::LOGIN_FIELD_NAME, $login);
        return $this->fillFields($res);
    }

    public function loadBySessId(string $sessId) 
    {
        $res = $this->_db->getRow(self::TABlE_NAME, self::SESS_FIELD_NAME, $sessId);
        return $this->fillFields($res);
    }

    private function fillFields($fieldsArray) {
        $this->_id = $fieldsArray[self::ID_FIELD_NAME];
        $this->_login = $fieldsArray[self::LOGIN_FIELD_NAME];
        $this->_password = $fieldsArray[self::PASSWORD_FIELD_NAME];
        $this->_amount = $fieldsArray[self::DEPO_FIELD_NAME];
        $this->_session = $fieldsArray[self::SESS_FIELD_NAME];
        return $this;
    }

    public function getData($field) 
    {
        $field = '_'.$field;
        return $this->$field ?? null;
    }

    public function setData($field, $value)
    {
        $field = '_'.$field;
        $this->$field = $value;
    }

    public function save()
    {
        foreach(self::SAVED_FIELDS as $field) {
            $this->_db->updateField(self::TABlE_NAME, $field, $this->getData($field), self::ID_FIELD_NAME, $this->getData(self::ID_FIELD_NAME));
        }
    }

}