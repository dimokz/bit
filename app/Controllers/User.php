<?php
namespace App\Controllers;

use Exception;

class User extends Controller
{

    private $_auth;
    private $_pay;
    private $_user;

    public function __construct($auth, $pay) {
        $this->_auth = $auth;
        $this->_pay = $pay;
    }

    public function loginAction()
    {
        $params = $this->getPostParams();
        if(isset($params['login']) && isset($params['password'])) {
            try {

                $this->_auth->login($params['login'], $params['password']);
                $this->viewAction("Успешная авторизация");
               
            } catch (Exception $e) {
               $this->viewAction("Ошибка авторизации");
            }
        } else {
            $this->viewAction("Неверный логин или пароль");
        }

    }

    public function logoutAction()
    {
        $this->_auth->logout();
        $this->viewAction("");
    }

    public function viewAction($message)
    {
        if($this->_auth->isLoged()) {
            $amount = $this->_auth->getUser()->getData("amount");
            require_once dirname(__FILE__) . "/../views/main.tpl.php";
        } else {
            require_once dirname(__FILE__) . "/../views/auth.tpl.php";
        }

    }

    /**
     * Экшн списания средств
     *
     * @return void
     */
    public function withdrawAction() 
    {   
        $params = $this->getPostParams();

        if($amount = filter_var($params['amount'], FILTER_VALIDATE_FLOAT)) {
            try {
                $this->_pay->withdraw($amount);
                $this->viewAction("Успешное списание");
            } catch (Exception $e) {
                $this->viewAction("Ошибка списания");
            }
        } else {
            $this->viewAction("Ошибка суммы");
        }
    }
}