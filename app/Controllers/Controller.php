<?php
namespace App\Controllers;

abstract class Controller {

    
    public function getPostParams()
    {

        if(!empty($_POST))
        {
          return $_POST;
        }
 
        $post = json_decode(file_get_contents('php://input'), true);
        if(json_last_error() == JSON_ERROR_NONE)
        {
            return $post;
        }

        return [];

    }
    
}