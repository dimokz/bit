<?php
session_start();

require_once dirname(__DIR__) . '/vendor/autoload.php';

use App\Core;

$core = new Core;
$core->run();

die;